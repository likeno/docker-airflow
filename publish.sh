IMAGE_URI=registry.gitlab.com/likeno/docker-airflow
AIRFLOW_GIT_REF=$1
IMAGE_SUB_VERSION=$(cat VERSION)

docker build \
    --build-arg AIRFLOW_GIT_REF=${AIRFLOW_GIT_REF} \
    --build-arg IMAGE_SUB_VERSION=${IMAGE_SUB_VERSION} \
    --tag ${IMAGE_URI}:${AIRFLOW_GIT_REF}-${IMAGE_SUB_VERSION} \
    .

docker push ${IMAGE_URI}:${AIRFLOW_GIT_REF}-${IMAGE_SUB_VERSION}
