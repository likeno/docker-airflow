# docker-airflow

Docker recipe for building an image with 
[apache-airflow](https://github.com/apache/incubator-airflow).

Specifics:

- The image is based on `ubuntu:16.04`;
- In order to maximize compatibility with trid party modules, it is 
  using `python2.7`
- Airflow is installed with support for:

  - `celery`
  - `redis`
  - `postgresql`
  - `crypto`
  - `password`


## Building docker images

Look in this repository's registry for pre-built images or clone it and build
it yourself. Supply the `AIRFLOW_VERSION` argument to docker build in order to
pull the desired airflow version from the 
[Python Package Index](https://pypi.python.org/pypi/apache-airflow)
