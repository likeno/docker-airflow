"""Update the airflow docker image."""
import argparse
import logging
import os
from subprocess import Popen
from subprocess import PIPE

logger = logging.getLogger(__name__)


def build_docker_image(image_tag, airflow_version, image_sub_version):
    command = [
        "docker", "build",
        "--build-arg", "AIRFLOW_VERSION={}".format(airflow_version),
        "--build-arg", "IMAGE_VERSION={}".format(image_sub_version),
        "--tag", image_tag,
        "--file", "Dockerfile",
        "."
    ]
    _execute_process(command)


def get_image_tag(image_uri, airflow_version, image_sub_version):
    return "{}:{}-{}".format(image_uri, airflow_version, image_sub_version)


def push_image(image_tag):
    _execute_process(["docker", "push", image_tag])


def _execute_process(command):
    process = Popen(command, stdout=PIPE, stderr=PIPE)
    while process.poll() is None:
        out = process.stdout.readline()
        logger.debug(out)
    else:
        if not int(process.returncode) == 0:
            raise RuntimeError(process.stderr.read())


def _get_image_sub_version():
    project_root = os.path.dirname(os.path.abspath(__file__))
    version_file_path = os.path.join(project_root, "VERSION")
    with open(version_file_path) as fh:
        version = fh.read().strip()
    return version


def _get_parser():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        "airflow_version",
        help="Version of apache airflow to build"
    )
    parser.add_argument(
        "-n",
        "--no_build_image",
        action="store_true",
        help="Do not build a fresh docker image"
    )
    parser.add_argument(
        "-p",
        "--push_to_registry",
        action="store_true",
        help="Push the built image to the remote docker registry"
    )
    parser.add_argument(
        "--base_image_uri",
        default="registry.gitlab.com/likeno/docker-airflow",
        help="Base URI for tagging the image"
    )
    parser.add_argument(
        "--verbose",
        action="store_true",
        help="Show more output"
    )
    return parser


if __name__ == "__main__":
    parser = _get_parser()
    args = parser.parse_args()
    logging.basicConfig(
        level=logging.DEBUG if args.verbose else logging.WARNING)
    logger.debug("Retrieving docker image version...")
    image_version = _get_image_sub_version()
    image_tag = get_image_tag(
        args.base_image_uri, args.airflow_version, image_version)
    if not args.no_build_image:
        logger.debug("Building docker image...")
        build_docker_image(image_tag, args.airflow_version, image_version)
    else:
        logger.debug("Not building a fresh docker image...")
    if args.push_to_registry:
        logger.debug("Pushing image to docker registry...")
        push_image(image_tag)
    print("Done!")
