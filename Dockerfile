FROM ubuntu:16.04

RUN adduser --uid 1000 --gecos '' --disabled-password airflow

ARG AIRFLOW_GIT_REF=1.8.2
ARG IMAGE_SUB_VERSION

ENV AIRFLOW_GIT_REF=${AIRFLOW_GIT_REF}
ENV IMAGE_SUB_VERSION=${IMAGE_SUB_VERSION}
ENV AIRFLOW_HOME=/home/airflow

LABEL pt.likeno.docker-airflow.version="${AIRFLOW_GIT_REF}-${IMAGE_SUB_VERSION}"
LABEL pt.likeno.docker-airflow.maintainer="ricardo.garcia.silva@likeno.pt"

COPY docker-entrypoint /usr/local/bin

WORKDIR /tmp

RUN apt-get update && apt-get install --yes \
    git \
    python-pip \
    libssl-dev \
    netcat \
  && pip install --upgrade \
    pip \
    setuptools \
    redis \
  && git clone https://github.com/apache/incubator-airflow.git airflow \
  && cd airflow \
  && git checkout ${AIRFLOW_GIT_REF} \
  && pip install .[celery,crypto,password,postgres] \
  && cd .. \
  && rm -rf airflow \
  && chmod 755 /usr/local/bin/docker-entrypoint \
  && mkdir -p /var/log/airflow \
  && chown airflow:airflow /var/log/airflow

WORKDIR /home/airflow

COPY airflow-default.cfg airflow.cfg

USER airflow

EXPOSE 8080 5555 8793

ENTRYPOINT ["docker-entrypoint"]

CMD ["webserver"]
